import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";

const styles = {
  stickToBottom: {
    width: "100%",
    position: "fixed",
    bottom: 0,
    height: 25,
    paddingLeft: 10
  }
};

const Footer = ({ classes }) => (
  <footer>
    <AppBar position="static" className={classes.stickToBottom}>
      <Typography color="inherit" align='center'>
        (c) Sportive Ultra Gold Year Limited Edition project
      </Typography>
    </AppBar>
  </footer>
);

Footer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Footer);
