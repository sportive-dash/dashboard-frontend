import React from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  styledLogo: { textAlign: "center", margin: "25px 0 0" }
};

const Logo = ({ classes }) => (
  <div className={classes.styledLogo}>
    <svg width="300" height="80">
      <g>
        <title>background</title>
        <rect
          fill="#ffffff"
          fillOpacity="0.0"
          id="canvas_background"
          height="82"
          width="302"
          y="-1"
          x="-1"
        />
        <g
          display="none"
          overflow="visible"
          y="0"
          x="0"
          height="100%"
          width="100%"
          id="canvasGrid"
        >
          <rect
            fill="url(#gridpattern)"
            strokeWidth="0"
            y="0"
            x="0"
            height="100%"
            width="100%"
          />
        </g>
      </g>
      <g>
        <title>Layer 1</title>
        <text
          stroke="#000"
          transform="matrix(2.465026835930672,0,0,2.180357055153166,-182.99788712616095,-175.80802272600798) "
          fontWeight="bold"
          textAnchor="start"
          fontFamily="'League Gothic', sans-serif"
          fontSize="24"
          id="svg_1"
          y="100.786647"
          x="74.943988"
          strokeWidth="0"
          fill="#000000"
        >
          SPORTIV
        </text>
        <line
          stroke="#000"
          strokeLinecap="undefined"
          strokeLinejoin="undefined"
          id="svg_4"
          y2="10.5"
          x2="292.355342"
          y1="10.5"
          x1="266"
          strokeWidth="7.5"
          fill="none"
        />
        <line
          stroke="#ff0255"
          strokeLinecap="undefined"
          strokeLinejoin="undefined"
          id="svg_7"
          y2="25.5"
          x2="286"
          y1="25.5"
          x1="271"
          strokeWidth="7"
          fill="none"
        />
        <ellipse
          ry="3"
          rx="2.5"
          id="svg_11"
          cy="25"
          cx="104"
          strokeWidth="8"
          stroke="#ff0255"
          fill="none"
        />
        <line
          stroke="#ff0255"
          strokeLinecap="null"
          strokeLinejoin="null"
          id="svg_12"
          y2="52.5"
          x2="295"
          y1="53.5"
          x1="4.999991"
          fillOpacity="null"
          strokeOpacity="null"
          fill="none"
        />
        <text
          fontStyle="normal"
          stroke="#ff0255"
          transform="matrix(0.8378846004790858,0,0,0.6865872819291211,79.29424882001209,68.56757908540965) "
          fontWeight="bold"
          textAnchor="start"
          fontFamily="Oswald, sans-serif"
          fontSize="24"
          id="svg_13"
          y="7.139861"
          x="-14.076124"
          fillOpacity="null"
          strokeOpacity="null"
          strokeWidth="0"
          fill="#000000"
        >
          DO IT TOGETHER
        </text>
        <line
          stroke="#000"
          strokeLinecap="undefined"
          strokeLinejoin="undefined"
          id="svg_15"
          y2="40.5"
          x2="292.355342"
          y1="40.5"
          x1="266"
          strokeWidth="7.5"
          fill="none"
        />
      </g>
    </svg>
  </div>
);

export default withStyles(styles)(Logo);
