import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Logo from "./Logo";
import MainMenu from "./MainMenu";

const styles = {
  root: { flexGrow: 1 }
};

function Header(props) {
  const { classes } = props;

  return (
    <header className={classes.root}>
      <AppBar position="static">
        <Logo />
        <MainMenu />
      </AppBar>
    </header>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
