import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const styles = theme => ({
  root: { minWidth: 150, margin: "0 20px" }
});

const MENU = ["Home", "Blog", "Dashboard", "Api", "KB"];

class MainMenu extends Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    return (
      <Tabs value={this.state.value} onChange={this.handleChange} centered>
        {MENU.map((menuItem, index) => {
          return <Tab key={index} classes={{root: classes.root}} label={menuItem} />;
        })}
      </Tabs>
    );
  }
}

MainMenu.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MainMenu);
