import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles/';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';
import './App.css';

const theme = createMuiTheme({
  palette: {
    primary: { main: '#1769aa' },
    secondary: { main: '#4dabf5' },
  },
});

export default () => (
  <MuiThemeProvider theme={theme}>
    <Header />
    <Main />
    <Footer />
  </MuiThemeProvider>
);
